﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MagicShop.Models;

namespace MagicShop.Abstract
{
    public interface IProductService
    {
        IEnumerable<ProductModel> GetProducts(ProductSearchModel productSearch);
        IEnumerable<ProductModel> GetProducts();
        void SaveProducts(ProductModel product);
        void DeleteProducts(ProductModel product);
        void UpdateProducts(ProductModel product);
    }
}
