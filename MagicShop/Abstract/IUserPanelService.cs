﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MagicShop.Models;

namespace MagicShop.Abstract
{
    public interface IUserPanelService
    {
        IEnumerable<UserModel> GetUsers();
        void SaveUser(UserModel userModel);
        void DeleteUser(UserModel userModel);
        void UpdateUser(UserModel userModel);
    }
}
