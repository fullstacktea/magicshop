﻿using MagicShop.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MagicShop.Abstract
{
	public interface IUserService
	{
		IEnumerable<UserModel> GetUsers();
		void SaveUser(UserModel userModel);
		void DeleteUser(UserModel userModel);
		void UpdateUser(UserModel userModel);
	}
}
