﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MagicShop.Models
{
    //public class ProductDescription
    //{
    //    public string TextDescription { get; set; }
    //    //public IFormFile[] Pictures { get; set; }
    //}

    //public class ProductParams
    //{
    //    //public float Size { get; set; } 
    //    public string Color { get; set; }
    //    public string Season { get; set; }
    //}

    //public class ProductModel
    //{
    //    public Guid Id { get; set; }
    //    public string Name { get; set; }
    //    public decimal Price { get; set; }
    //    //public string Category { get; set; }
    //    public uint Quantity { get; set; }
    //    //public ShippingOptions ShippingOptions { get; set; }
    //    public ProductParams ProductParams { get; set; }
    //    public ProductDescription ProductDescription { get; set; }
    //}


    public class ProductModel
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public decimal Price { get; set; }
        public uint Quantity { get; set; }
        public string Color { get; set; }

        public string CreatedOn { get; set; }
        public bool IsDeleted { get; set; }
        public string Season { get; set; }
        public string TextDescription { get; set; }
    }
}
