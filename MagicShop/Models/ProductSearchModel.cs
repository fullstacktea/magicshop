﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MagicShop.Models
{
    public class ProductSearchModel
    {
        public string Name { get; set; }

        public int PageSize { get; set; }
        public int PageIndex { get; set; }
    }
}
