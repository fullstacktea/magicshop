﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MagicShop.Models
{
	public class UserModel
	{
		public Guid Id { get; set; }
		public string FirstName { get; set; }
		public string LastName { get; set; }
		public string Email { get; set; }
		public string Password { get; set; }
		public DateTime? DateOfBirth { get; set; }
		public string PhoneNumber { get; set; }
		public bool TermsAgreement { get; set; }
		public string Street { get; set; }
		public string HouseNumber { get; set; }
		public string ApartamentNumber { get; set; }
		public string PostalCode { get; set; }
		public string City { get; set; }
		public string Country { get; set; }

	}
}
