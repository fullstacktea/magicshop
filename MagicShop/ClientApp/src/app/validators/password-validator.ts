import {Directive} from "@angular/core";
import { AbstractControl, NG_VALIDATORS, FormControl, Validator , FormGroup } from "@angular/forms";

export function passwordValidator(abstractControl : AbstractControl)
{
let strongPassword = new RegExp ("^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#\$%\^&\*])(?=.{8,})");
const valid = strongPassword.test(abstractControl.value);
return valid ? null : Error;
}
