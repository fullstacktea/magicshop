import { FormGroupDirective } from "@angular/forms";

export class UserModel {
  id: string;
  firstName: string;
  lastName: string;
  email: string;
  password: string;
  dateOfBirth: Date;
  phoneNumber: string;
  termsAgreement: boolean;
  street: string;
  houseNumber: string;
  apartamentNumber: string;
  postalCode: string;
  city: string;
  country: string;
}
