export class ProductSearch {
  name: string;
  pageSize: number;
  pageIndex: number;
}
