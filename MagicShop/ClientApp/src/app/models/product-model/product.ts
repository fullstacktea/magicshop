import { ProductDescription } from "./product-description";
import { ProductParams } from "./product-params";

export class Product{
    textDescription: string = "";
    name: string = "";
    price: number = 0;
    quantity: number = 0;
    season: string = "";
    // params: ProductParams = new ProductParams();
    color: string = "";
}
