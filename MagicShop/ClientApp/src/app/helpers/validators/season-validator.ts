import { AbstractControl } from '@angular/forms';

export function seasonValidator(
  control: AbstractControl
) {
  if(!control.value) {
    return {'seasonEmptyError': true}
  }
  let seasonsArray = ["Summer", "Spring", "Winter", "Autumn"]
  if(!seasonsArray.includes(control.value)){
    return {'seasonWrongValueError': true}
  }
}
