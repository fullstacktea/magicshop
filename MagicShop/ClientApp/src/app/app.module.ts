import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule} from '@angular/common/http';
import { RouterModule } from '@angular/router';

import { AppComponent } from './app.component';
import { NavMenuComponent } from './nav-menu/nav-menu.component';
import { HomeComponent } from './home/home.component';
import { CounterComponent } from './counter/counter.component';
import { FetchDataComponent } from './fetch-data/fetch-data.component';
import { UserPanelComponent } from './pages/user-panel/user-panel.component';
import { AddProductComponent } from './pages/add-product/add-product.component';
import { EditionPanelComponent } from './pages/edition-panel/edition-panel.component';
import { ProductListComponent } from './pages/product-list/product-list.component';
import { ProductPreviewComponent, AddToCartDialogComponent } from './pages/product-list/product-preview/product-preview.component';

import {MatCheckboxModule} from '@angular/material/checkbox';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatButtonModule } from '@angular/material/button';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { MatSelectModule } from '@angular/material/select';
import { CategoryComponent } from './pages/category/category.component';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatCardModule } from '@angular/material/card';
import { MatDialogModule } from '@angular/material/dialog';
import {MatGridListModule} from '@angular/material/grid-list';
import {MatIconModule} from '@angular/material/icon';
import {MatNativeDateModule} from '@angular/material/core';
import { MatMomentDateModule } from "@angular/material-moment-adapter";
import { ProductEditionPanelComponent } from './pages/product-edition-panel/product-edition-panel.component';
import { MatPaginatorModule } from '@angular/material/paginator';
import { FooterComponent } from './footer/footer.component';
import {MatToolbarModule} from '@angular/material/toolbar';


@NgModule({
	declarations: [
		AppComponent,
		NavMenuComponent,
		HomeComponent,
		CounterComponent,
		FetchDataComponent,
		UserPanelComponent,
		AddProductComponent,
		EditionPanelComponent,
		CategoryComponent,
		ProductListComponent,
		ProductPreviewComponent,
		AddToCartDialogComponent,
		ProductEditionPanelComponent,
    FooterComponent

	],
  imports: [
    BrowserModule.withServerTransition({appId: 'ng-cli-universal'}),
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule.forRoot([
      {path: '', component: HomeComponent, pathMatch: 'full'},
      {path: 'counter', component: CounterComponent},
      {path: 'fetch-data', component: FetchDataComponent},
      {path: 'user-panel', component: UserPanelComponent},
      {path: 'add-product', component: AddProductComponent},
      {path: 'edition-panel', component: EditionPanelComponent},
      {path: 'category', component: CategoryComponent},
      {path: 'product-list', component: ProductListComponent},
      {path: 'edit-products', component: ProductEditionPanelComponent}
    ], {relativeLinkResolution: 'legacy'}),
    MatButtonModule,
    BrowserAnimationsModule,
    ReactiveFormsModule,
    MatDatepickerModule,
    MatFormFieldModule,
    MatInputModule,
    MatSelectModule,
    MatCardModule,
    MatDialogModule,
    MatGridListModule,
    MatNativeDateModule,
    MatDatepickerModule,
    MatButtonModule,
    MatMomentDateModule,
    MatIconModule,
    MatCheckboxModule,
    MatPaginatorModule,
    MatToolbarModule
  ],
	providers: [],
	entryComponents: [
		ProductPreviewComponent,
		AddToCartDialogComponent

	],
	bootstrap: [AppComponent],

})
export class AppModule { }
