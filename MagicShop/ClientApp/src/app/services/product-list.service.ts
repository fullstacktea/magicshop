import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Product } from '../models/product-model/product';
import { ProductSearch } from '../models/product-model/product-search';

const baseUrl = 'https://localhost:44328/';


@Injectable({
  providedIn: 'root'
})

export class ProductListService {

  constructor(private http: HttpClient) { }

  getProducts(): Observable<Product[]> {
    return this.http.get<Product[]>(baseUrl + 'api/product/getProducts');
  }

  getProductsByModel(ProductSearch: ProductSearch): Observable<Product[]> {
    return this.http.post<Product[]>(baseUrl + 'api/product/getProducts', ProductSearch);
  }

  saveProduct(product: Product): Observable<Product> {
    return this.http.post<Product>(baseUrl + 'api/product/saveProduct', product);
  }

  deleteProduct(product: Product): Observable<Product>{
    return this.http.post<Product>(baseUrl + 'api/product/deleteProduct', product);
  }

  updateProduct(product: Product): Observable<Product>{
    return this.http.post<Product>(baseUrl + 'api/product/updateProduct', product);
  }

}
