import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { UserModel } from '../models/user-model';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  constructor(private httpClient: HttpClient) { }

  getUsers(): Observable<UserModel[]>{
    return this.httpClient.get<UserModel[]>(environment.baseLink + 'api/user/getUsers');
  }

  saveUser(user: UserModel): Observable<UserModel>{
    return this.httpClient.post<UserModel>(environment.baseLink + 'api/user/saveUser', user);
  }

  deleteUser(user: UserModel): Observable<UserModel>{
    return this.httpClient.post<UserModel>(environment.baseLink + 'api/user/deleteUser', user);
  }

  updateUser(user: UserModel): Observable<UserModel>{
    return this.httpClient.post<UserModel>(environment.baseLink + 'api/user/updateUser', user);
  }
}
