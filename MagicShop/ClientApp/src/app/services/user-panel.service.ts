import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { UserModel } from '../models/user-model';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class UserPanelService {

  constructor (private httpClient: HttpClient) { }


  getUsers(): Observable<UserModel[]>{
    return this.httpClient.get<UserModel[]>(environment.baseLink + 'api/userPanel/getUsers');
  }

  saveUser(user: UserModel): Observable<UserModel>{
    return this.httpClient.post<UserModel>(environment.baseLink + 'api/userPanel/saveUser', user);
  }

  deleteUser(user: UserModel): Observable<UserModel>{
    return this.httpClient.post<UserModel>(environment.baseLink + 'api/userPanel/deleteUser', user);
  }

  updateUser(user: UserModel): Observable<UserModel>{
    return this.httpClient.post<UserModel>(environment.baseLink + 'api/userPanel/updateUser', user);
  }
}
