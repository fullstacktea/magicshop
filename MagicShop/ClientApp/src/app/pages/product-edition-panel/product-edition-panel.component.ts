import { Component, OnInit } from '@angular/core';
import { Product } from 'src/app/models/product-model/product';
import { ProductListService } from '../../services/product-list.service';

@Component({
  selector: 'app-product-edition-panel',
  templateUrl: './product-edition-panel.component.html',
  styleUrls: ['./product-edition-panel.component.css']
})
export class ProductEditionPanelComponent implements OnInit {

  products: Product[];

  constructor(private productListService: ProductListService) { }

  ngOnInit(): void {
    this.productListService.getProducts().subscribe(data => {
      this.products = data;

    });
  }

  updateProduct(product): void {
    this.productListService.updateProduct(product).subscribe(
      response => {
        window.alert("Product updated");
        console.debug(response);
      },
      error => {
        console.debug(error);
      }
    );
  }

  deleteProduct(product: Product): void {
    this.productListService.deleteProduct(product).subscribe(
      response => {
        window.alert("Product deleted");
        console.debug(response);
      },
      error => {
        console.debug(error);
      }
    );
  }



}
