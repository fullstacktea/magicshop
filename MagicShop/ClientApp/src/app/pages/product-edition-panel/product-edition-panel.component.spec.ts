import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ProductEditionPanelComponent } from './product-edition-panel.component';

describe('ProductEditionPanelComponent', () => {
  let component: ProductEditionPanelComponent;
  let fixture: ComponentFixture<ProductEditionPanelComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ProductEditionPanelComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ProductEditionPanelComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
