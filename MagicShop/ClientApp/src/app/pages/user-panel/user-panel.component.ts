import { Component, OnInit, } from '@angular/core';
import { FormGroup, FormControl, Validators, AbstractControl, FormBuilder} from '@angular/forms';
import { UserService } from 'src/app/services/user.service';
import { UserPanelService } from 'src/app/services/user-panel.service';
import { passwordValidator } from 'src/app/validators/password-validator';
import { DatePipe } from '@angular/common';

export function validator(control: AbstractControl) {
if(control.value =='')
{return false;}}

@Component({
  selector: 'app-user-panel',
  templateUrl: './user-panel.component.html',
  styleUrls: ['./user-panel.component.css']
})
export class UserPanelComponent implements OnInit {

  hide = true;

  pipe = new DatePipe('en-US');
  now = Date.now();
  mySimpleFormat = this.pipe.transform(this.now, 'dd/mm/yyyy');
  minDate = new Date(1900, 1, 1);
  maxDate = new Date(2030, 1, 1);

  constructor(private userService: UserService,
    private formBuilder: FormBuilder,
    private userPanelService : UserPanelService) {}

  profileForm = this.formBuilder.group
    ({
      firstName: [null, [Validators.required, validator]],
      lastName:  [null, [Validators.required, validator]],
      email: [null, [Validators.required, validator, Validators.pattern('^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+.[a-zA-Z0-9-.]+$')]],
      password: [null, [Validators.required, passwordValidator]],
      confirmPassword:[null, [Validators.required ]],
      phoneNumber: [null, [Validators.required, validator, Validators.pattern('[- +()0-9]+')]],
      dateOfBirth: [null],
      termsAgreement: [null,[Validators.required]],
    },{validator: this.checkIfMatchingPasswords('password', 'confirmPassword')});

    ngOnInit() {

    }

    checkIfMatchingPasswords(passwordKey: string, passwordConfirmationKey: string) {
      return (group: FormGroup) => {
        let passwordInput = group.controls[passwordKey],
            passwordConfirmationInput = group.controls[passwordConfirmationKey];
        if (passwordInput.value !== passwordConfirmationInput.value) {
          return passwordConfirmationInput.setErrors({notEquivalent: true})
        }
        else {
            return passwordConfirmationInput.setErrors(null);
        }
      }
    }

    onClickMe()
    {
      console.log(this.profileForm.controls["password"].valid);
      console.log(this.profileForm.controls["confirmPassword"].valid);
      this.userPanelService.saveUser(this.profileForm.value).subscribe(()=>{}, error =>{});
    }
    onSubmit()
    {}
    }

