import { Component, OnInit } from '@angular/core';
import { Category } from 'src/app/models/category';
import { FormArray, FormBuilder } from '@angular/forms';

@Component({
  selector: 'app-category',
  templateUrl: './category.component.html',
  styleUrls: ['./category.component.css']
})
export class CategoryComponent implements OnInit {

  addCategoryForm = this.formBuilder.group({
    name: [''],
    icon: [''],
    size: this.formBuilder.group({
      type: [''],
      options: this.formBuilder.array([
        this.formBuilder.control('')
      ])
    }),
  });

  constructor(private formBuilder: FormBuilder) { }

  get options() {
    return this.addCategoryForm.get('size').get('options') as FormArray;
  }

  addOption() {
    console.log("Adding option");
    this.options.push(this.formBuilder.control(''));
  }

  removeOption(index: number) {
    console.log("Removing option");
    this.options.removeAt(index);
  }

  onSubmit(){
    console.info("Data from this form is ready to be sent on backend");
  }

  ngOnInit() {
  }

  addCategory() {

  }
}
