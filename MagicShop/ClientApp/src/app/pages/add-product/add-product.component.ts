import { Component, OnInit } from '@angular/core';
import { FormArray, FormBuilder } from '@angular/forms';
import { Product } from '../../models/product-model/product';
import { seasonValidator } from 'src/app/helpers/validators/season-validator';
import { Validators } from "@angular/forms";
import {ProductListService} from "../../services/product-list.service";

@Component({
  selector: 'app-add-product',
  templateUrl: './add-product.component.html',
  styleUrls: ['./add-product.component.css']
})
export class AddProductComponent implements OnInit {

  addProductForm = this.formBuilder.group({
    name: ['', Validators.required],
    price: ['', Validators.required],
    quantityInStock: ['', Validators.required],
    category: ['', Validators.required],
    params: this.formBuilder.group({
      size: ['', Validators.required],
      color: ['', Validators.required],
      season: ['', seasonValidator]
    }),
    description: this.formBuilder.group({
      textDescription: ['', Validators.required]
      // pictures: this.formBuilder.array([
      //   this.formBuilder.control('', Validators.required)
      // ])
    })
  });

  get pictures() {
    return this.addProductForm.get('description').get('pictures') as FormArray;
  }

  addImage() {
    this.pictures.push(this.formBuilder.control(''));
  }

  removeImage(index: number) {
    this.pictures.removeAt(index);
  }

  constructor(
    private formBuilder: FormBuilder,
    private productService: ProductListService)
  {
  }

  ngOnInit(){
  }

  onSubmit(){
    const data : Product = {
      textDescription: this.addProductForm.get('description').get('textDescription').value,
      name: this.addProductForm.get('name').value,
      price: this.addProductForm.get('price').value,
      quantity: this.addProductForm.get('quantityInStock').value,
      color: this.addProductForm.get('params').get('color').value,
      season: this.addProductForm.get('params').get('season').value
    };

    this.productService.saveProduct(data).subscribe(
      response => {
        window.alert("Product added");
        console.debug(response);
      },
      error => {
        console.debug(error);
      });
  }

}
