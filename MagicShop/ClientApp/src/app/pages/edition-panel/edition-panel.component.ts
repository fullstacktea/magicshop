import { Component } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';

@Component({
	selector: 'app-edition-panel',
	templateUrl: './edition-panel.component.html',
	styleUrls: ['./edition-panel.component.css']
})
export class EditionPanelComponent {
	PanelEditionForm = new FormGroup({
		name: new FormControl(''),
		surname: new FormControl(''),
		adress: new FormControl(''),
		phone_number: new FormControl(''),
		e_mail: new FormControl(''),
		payment_method: new FormControl('')

	});

	onSubmit() {
		console.warn(this.PanelEditionForm.value);
	}

}
