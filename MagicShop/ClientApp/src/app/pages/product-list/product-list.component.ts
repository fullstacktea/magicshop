import {
  Component,
  ViewChild,
  ViewContainerRef,
  ComponentFactoryResolver,
  OnInit
} from '@angular/core';
import { PageEvent, MatPaginator } from '@angular/material/paginator';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { ProductListService } from '../../services/product-list.service';
import { ProductPreviewComponent } from './product-preview/product-preview.component';
import { ProductSearch } from '../../models/product-model/product-search';


@Component({
	selector: 'app-product-list',
	templateUrl: './product-list.component.html',
	styleUrls: ['./product-list.component.styl']
})


export class ProductListComponent implements OnInit {

  @ViewChild('productPreview', { read: ViewContainerRef }) entry: ViewContainerRef;
  @ViewChild(MatPaginator) paginator: MatPaginator;

  constructor(private productListService: ProductListService, private resolver: ComponentFactoryResolver) {
  }




	listFilterForm = new FormGroup({
		title: new FormControl(''),
		size: new FormControl(''),
		color: new FormControl(''),
		sex: new FormControl(''),
		lowestPrice: new FormControl(0, Validators.pattern('^[0-9]*$')),
		highestPrice: new FormControl(999, Validators.pattern('^[0-9]*$'))
	});

  searchModel = new ProductSearch();
  // Page pagination settings
  pageLength: number;
  pageSize = 8;
  pageEvent: PageEvent;
  //


  onClick() {
    const res = this.listFilterForm.value;
    this.searchModel.name = res.title;
    this.paginator.pageIndex = 0;
    this.searchModel.pageIndex = this.paginator.pageIndex;

    this.showProductList();

  }

  showProductList() {

    this.entry.clear();
    this.productListService.getProductsByModel(this.searchModel).subscribe(data => {

      this.pageLength = data.length;

      for (let item in data) {
        if (data[item]) // TODO: Should be replaced
          this.createComponent(data[item]);
      }
    });

  }

  ngOnInit(): void {
    this.searchModel.pageSize = this.pageSize;

  }

  onPaginateChange(event: PageEvent): void {
    this.searchModel.pageIndex = event.pageIndex;

    this.showProductList();

  }

  createComponent(product) {
    const factory = this.resolver.resolveComponentFactory(ProductPreviewComponent);
    const componentRef = this.entry.createComponent(factory);
    componentRef.instance.product = product;
  }
}




