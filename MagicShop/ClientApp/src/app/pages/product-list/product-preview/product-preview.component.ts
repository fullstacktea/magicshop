import { Component, OnInit, Input } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { ProductListService } from '../../../services/product-list.service';
import { Product } from '../../../models/product-model/product';
import { ProductDescription } from '../../../models/product-model/product-description';


@Component({
  selector: 'app-product-preview',
  templateUrl: './product-preview.component.html',
  styleUrls: ['./product-preview.component.styl']
})
export class ProductPreviewComponent implements OnInit {

  @Input() product: Product;

  img = "https://i.pinimg.com/474x/69/77/b7/6977b70a129ec184527433bbdf9fe457.jpg";

  openDialog() {
    this.dialog.open(AddToCartDialogComponent);
    console.log(this.product.name + " ready to be sent to backend");

  }
  constructor(public dialog: MatDialog, private productListService : ProductListService) { }
  ngOnInit() {
  }

}

@Component({
  selector: 'app-add-to-cart-dialog',
  templateUrl: 'add-to-cart-dialog.html',
})
export class AddToCartDialogComponent { }
