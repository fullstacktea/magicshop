﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Database;
using MagicShop.Abstract;
using MagicShop.Models;

namespace MagicShop.Implementation
{
    public class UserPanelService : IUserPanelService
    {
        private readonly MagicShopDbContext _magicShopDbContext;
        private readonly IMapper _mapper;
        public UserPanelService(MagicShopDbContext formDbContext, IMapper mapper)
        {
            _magicShopDbContext = formDbContext;
            _mapper = mapper;
        }
        public void DeleteUser(UserModel userModel)
        {
            var userToRemove = _magicShopDbContext.Users.FirstOrDefault(user => user.Id == userModel.Id);
            if (userModel != null)
            {
                _magicShopDbContext.Users.Remove(userToRemove);
                _magicShopDbContext.SaveChanges();
            }
        }

        public IEnumerable<UserModel> GetUsers()
        {
            var users = _mapper.Map<IEnumerable<UserModel>>(_magicShopDbContext.Users).ToList();
            return users;
        }
        public void SaveUser (UserModel userModel)
        {
            _magicShopDbContext.Users.Add(new Entities.User
            {
                Id = Guid.NewGuid(),
                FirstName = userModel.FirstName,
                LastName = userModel.LastName,
                Email = userModel.Email,
                Password = userModel.Password,
                DateOfBirth = userModel.DateOfBirth,
                PhoneNumber = userModel.PhoneNumber,
                TermsAgreement = userModel.TermsAgreement,
                Street = userModel.Street,
                HouseNumber = userModel.HouseNumber,
                ApartamentNumber = userModel.ApartamentNumber,
                PostalCode = userModel.PostalCode,
                City = userModel.City,
                Country = userModel.Country,
               
            });
            _magicShopDbContext.SaveChanges();
        }
        public void UpdateUser (UserModel userModel)
        {
            var userToChange = _magicShopDbContext.Users.FirstOrDefault(user => user.Id == userModel.Id);
            userToChange.FirstName = userModel.FirstName;
            userToChange.LastName = userModel.LastName;
            userToChange.Email = userModel.Email;
            _magicShopDbContext.SaveChanges();
        }
    }
}
