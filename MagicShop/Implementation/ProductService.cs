﻿using AutoMapper;
using Database;
using Entities;
using LinqKit;
using MagicShop.Abstract;
using MagicShop.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace MagicShop.Implementation
{
    public class ProductService : IProductService
    {
        private readonly MagicShopDbContext _magicShopDbContext;
        private readonly IMapper _mapper;
        
        public ProductService(MagicShopDbContext formDbContext, IMapper mapper)
        {
            _magicShopDbContext = formDbContext;
            _mapper = mapper;
        }

        public void DeleteProducts(ProductModel productModel)
        {
            var productToRemove = _magicShopDbContext.Products.FirstOrDefault(product => product.Id == productModel.Id);
            if (productModel != null)
            {
                _magicShopDbContext.Products.Remove(productToRemove);
                _magicShopDbContext.SaveChanges();
            }
        }

        public IEnumerable<ProductModel> GetProducts(ProductSearchModel productSearch)
        {
            ExpressionStarter<Product> predicate = GetSearchCondition(productSearch);

            int startIndex = productSearch.PageSize * ( productSearch.PageIndex );

            var productEntities = _magicShopDbContext.Products.Where(predicate);
            int count = productEntities.Count();
            productEntities = productEntities.Skip(startIndex).Take(productSearch.PageSize); // select products from range
            var products = _mapper.Map<IEnumerable<ProductModel>>(productEntities).ToList();

            for (int i = products.Count; i < count; i++)
            {
                products.Add(null); // added pseudosize for pagination
            }
            

            return products;
        }

        public IEnumerable<ProductModel> GetProducts()
        {


            var productEntities = _magicShopDbContext.Products;
            var products = _mapper.Map<IEnumerable<ProductModel>>(productEntities).ToList();

            return products;
        }



        private static ExpressionStarter<Product> GetSearchCondition(ProductSearchModel productSearch)
        {
            var predicate = PredicateBuilder.New<Product>(true);

            if (!string.IsNullOrEmpty(productSearch.Name))
            {
                Expression<Func<Product, bool>> expression = c => c.Name.Contains(productSearch.Name);
                predicate.Or(expression);
            }

            return predicate;
        }

        public void SaveProducts(ProductModel product)
        {
            _magicShopDbContext.Products.Add(new Entities.Product
            {
                Id = Guid.NewGuid(),
                Name = product.Name,
                Price = product.Price,
                Quantity = (int)product.Quantity,
                Color = product.Color,
                Season = product.Season,
                TextDescription = product.TextDescription
            });
            _magicShopDbContext.SaveChanges();
        }

        public void UpdateProducts(ProductModel productModel)
        {
            var productToUpdate = _magicShopDbContext.Products.FirstOrDefault(product => product.Id == productModel.Id);
            if(productToUpdate != null)
            {
                productToUpdate.Name = productModel.Name;
                productToUpdate.Price = productModel.Price;
                productToUpdate.Color = productModel.Color;
                productToUpdate.Quantity = (int)productModel.Quantity;
                productToUpdate.Season = productModel.Season;
                productToUpdate.TextDescription = productModel.TextDescription;

                _magicShopDbContext.SaveChanges();
            }
        }
    }
}
