﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Cors;
using MagicShop.Abstract;
using MagicShop.Models;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace MagicShop.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ProductController : ControllerBase
    {
        private readonly IProductService _productService;
        public ProductController(IProductService productService)
        {
            _productService = productService;
        }

        [HttpPost("getProducts")]
        public async Task<IEnumerable<ProductModel>> GetProducts(ProductSearchModel productSearch)
        {
            var result = _productService.GetProducts(productSearch);

            return result;
        }

        [HttpGet("getProducts")]
        public async Task<IEnumerable<ProductModel>> GetProducts()
        {
            var result = _productService.GetProducts();

            return result;
        }

        [HttpPost("deleteProduct")]
        public void DeleteProduct([FromBody] ProductModel product)
        {
            _productService.DeleteProducts(product);
        }


        [HttpPost("saveProduct")]
        public void SaveProduct([FromBody] ProductModel product)
        {
            _productService.SaveProducts(product);
        }

        [HttpPost("updateProduct")]
        public void UpdateProduct([FromBody] ProductModel product)
        {
            _productService.UpdateProducts(product);
        }


    }
}
