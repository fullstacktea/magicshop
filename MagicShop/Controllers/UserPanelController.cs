﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MagicShop.Abstract;
using MagicShop.Models;
using Microsoft.AspNetCore.Http;

namespace MagicShop.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UserPanelController : ControllerBase
    {
        private readonly IUserPanelService _userPanelService;
        public UserPanelController(IUserPanelService userPanelService)
        {
         
            _userPanelService = userPanelService;
        }
        [HttpGet("getUsers")]
        public async Task<IEnumerable<UserModel>> GetUser()
        {
            var result = _userPanelService.GetUsers();
            return result;
        }
        [HttpPost("deleteUser")]
        public void DeleteUser([FromBody] UserModel userModel)
        {
            _userPanelService.DeleteUser(userModel);
        }
        [HttpPost("saveUser")]
        public async Task SaveUser([FromBody] UserModel userModel)
        {
            _userPanelService.SaveUser(userModel);
        }


        [HttpGet]
        public IEnumerable<string> Get()
        {
            var names = new List<string> { "Marta", "Ana", "Felipe" };
            return names; 
        }

        [HttpPost("updateUser")]
        public void UpdateUser([FromBody] UserModel userModel)
        {
            _userPanelService.UpdateUser(userModel);
        }
    }
}