﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MagicShop.Abstract;
using MagicShop.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace MagicShop.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UserController : ControllerBase
    {
		private readonly IUserService _userService;
		public UserController(IUserService userService)
		{
			_userService = userService;
		}

		[HttpGet("getUsers")]
		public async Task<IEnumerable<UserModel>> GetUsers()
		{
			var result = _userService.GetUsers();

			return result;
		}

		[HttpPost("deleteUser")]
		public void DeleteUser([FromBody] UserModel userModel)
		{
			_userService.DeleteUser(userModel);
		}

		[HttpPost("saveUser")]
		public void SaveUser([FromBody] UserModel userModel)
		{
			_userService.SaveUser(userModel);
		}

		[HttpPost("updateUser")]
		public void UpdateUser([FromBody] UserModel userModel)
		{
			_userService.UpdateUser(userModel);
		}
	}
}
