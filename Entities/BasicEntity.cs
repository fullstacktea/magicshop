﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Entities
{
	public abstract class BasicEntity
	{
		public Guid Id { get; set; }
		public DateTime CreatedOn { get; set; }
		public bool IsDeleted { get; set; }
	}
}
