﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Entities
{
	public class Product : BasicEntity
	{
		public string Name { get; set; }
		[Column(TypeName = "decimal(18,4)")]
		public decimal Price { get; set; }
		//public Guid CategoryId { get; set; }
		//public virtual Category Category { get; set; }
		public int Quantity { get; set; }
		public string Color { get; set; }
		public string Season { get; set; }
		public string TextDescription { get; set; }
	}
}
