﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Entities
{
	public class Category : BasicEntity
	{
		public string Name { get; set; }
	}
}
